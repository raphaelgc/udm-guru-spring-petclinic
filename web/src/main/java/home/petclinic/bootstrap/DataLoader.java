package home.petclinic.bootstrap;

import home.petclinic.model.Owner;
import home.petclinic.model.Pet;
import home.petclinic.model.PetType;
import home.petclinic.model.Vet;
import home.petclinic.services.OwnerService;
import home.petclinic.services.PetTypeService;
import home.petclinic.services.VetService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class DataLoader implements CommandLineRunner {

    private final OwnerService ownerService;
    private final VetService vetService;
    private final PetTypeService petTypeService;

    public DataLoader(OwnerService ownerService, VetService vetService, PetTypeService petTypeService) {
        this.ownerService = ownerService;
        this.vetService = vetService;
        this.petTypeService = petTypeService;
    }

    @Override
    public void run(String... args) throws Exception {

        PetType dog = new PetType();
        dog.setName("dog");
        PetType savedDogType = petTypeService.save(dog);

        PetType cat = new PetType();
        dog.setName("cat");
        PetType savedCatType = petTypeService.save(cat);

        Owner owner1 = new Owner();
        owner1.setFirstName("Pedro");
        owner1.setLastName("Silva");
        owner1.setAddress("address 1");
        owner1.setCity("city 1");
        owner1.setTelephone("tel 1");

        Pet mikesPet = new Pet();
        mikesPet.setPetType(savedDogType);
        mikesPet.setOwner(owner1);
        mikesPet.setBirthDate(LocalDate.now());
        owner1.getPets().add(mikesPet);

        ownerService.save(owner1);

        Owner owner2 = new Owner();
        owner2.setFirstName("Joao");
        owner2.setLastName("Marques");
        owner2.setAddress("address 2");
        owner2.setCity("city 2");
        owner2.setTelephone("tel 2");

        Pet joaoPet = new Pet();
        joaoPet.setPetType(savedCatType);
        joaoPet.setOwner(owner2);
        joaoPet.setBirthDate(LocalDate.now());
        owner2.getPets().add(joaoPet);

        ownerService.save(owner2);

        Vet vet1 = new Vet();
        vet1.setFirstName("Maria");
        vet1.setLastName("Rosa");
        vetService.save(vet1);

        Vet vet2 = new Vet();
        vet2.setFirstName("Julia");
        vet2.setLastName("Maria");
        vetService.save(vet2);


    }

}
