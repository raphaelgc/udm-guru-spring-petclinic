package home.petclinic.services;

import home.petclinic.model.PetType;

public interface PetTypeService extends CrudService<PetType, Long>  {
}
