package home.petclinic.services.map;

import home.petclinic.model.Owner;
import home.petclinic.model.Pet;
import home.petclinic.services.OwnerService;
import home.petclinic.services.PetService;
import home.petclinic.services.PetTypeService;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Set;

@Service
public class OwnerServiceMap extends AbstractMapService<Owner, Long> implements OwnerService {

    private final PetTypeService petTypeService;
    private final PetService petService;

    public OwnerServiceMap(PetTypeService petTypeService, PetService petService) {
        this.petTypeService = petTypeService;
        this.petService = petService;
    }

    @Override
    public Set<Owner> findAll() {
        return super.findAll();
    }

    @Override
    public Owner save(Owner object) {
        if(Objects.nonNull(object)){
            if (Objects.nonNull(object.getPets())){
                object.getPets().forEach(pet -> {
                    if(Objects.nonNull(pet.getPetType())){
                        if(Objects.isNull(pet.getPetType().getId())){
                            pet.setPetType(petTypeService.save(pet.getPetType()));
                        }
                    } else {
                        throw new RuntimeException("Pet type is required!");
                    }
                    if (Objects.isNull(pet.getId())){
                        Pet savedPet = petService.save(pet);
                        pet.setId(savedPet.getId());
                    }
                });
            }
            return super.save(object);
        } else {
            return null;
        }
    }

    @Override
    public void delete(Owner object) {
        super.delete(object);
    }

    @Override
    public void deleteByID(Long id) {
        super.deleteById(id);
    }

    @Override
    public Owner findById(Long id) {
        return super.findById(id);
    }

    @Override
    public Owner findByLastName(String lastName) {
        return null;
    }
}
