package home.petclinic.services.map;

import home.petclinic.model.Specialilty;
import home.petclinic.services.SpecialityService;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class SpecialityServiceMap extends AbstractMapService<Specialilty, Long> implements SpecialityService {
    @Override
    public Set<Specialilty> findAll() {
        return super.findAll();
    }

    @Override
    public Specialilty findById(Long id) {
        return super.findById(id);
    }

    @Override
    public void deleteByID(Long id) {
        super.deleteById(id);
    }

    @Override
    public void delete(Specialilty object) {
        super.save(object);
    }

    @Override
    public Specialilty save(Specialilty object) {
        return super.save(object);
    }
}
