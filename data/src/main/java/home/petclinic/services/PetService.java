package home.petclinic.services;

import home.petclinic.model.Owner;
import home.petclinic.model.Pet;

import java.util.Set;

public interface PetService extends CrudService<Pet, Long>{

}
