package home.petclinic.services;

import home.petclinic.model.Pet;
import home.petclinic.model.Vet;

import java.util.Set;

public interface VetService extends CrudService<Vet, Long>{

}
