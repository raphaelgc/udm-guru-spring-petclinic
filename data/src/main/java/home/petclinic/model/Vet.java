package home.petclinic.model;

import java.util.HashSet;
import java.util.Set;

public class Vet extends Person{

    private Set<Specialilty> specialilies = new HashSet<>();

    public Set<Specialilty> getSpecialilies() {
        return specialilies;
    }

    public void setSpecialilies(Set<Specialilty> specialilies) {
        this.specialilies = specialilies;
    }
}
